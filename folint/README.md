FOLint 
======

FOLint is a linting tool for [FO(·)](https://fo-dot.readthedocs.io/en/latest/FO-dot.html).
Its functionality can be split up in four parts:

* Detection of syntax errors
* Detection of type errors
* Detection of typical formalization errors
* Enforcing a style guide

This project was initially started by Lars Vermeulen during his master thesis: https://github.com/larsver/folint


Installation
------------

```
pip install folint
```

CLI usage
---------

```
folint file.idp
```

FOLint in editors
-----------------

FOLint can be integrated in many editors. 
A collection of guides are kept in the folint-in-editors folder.
